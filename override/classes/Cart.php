<?php
/**
 * Korel
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact levikore93@gmail.com
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */

class Cart extends CartCore
{
    public function checkQuantities($returnProductOnFailure = false)
    {
        require_once _PS_MODULE_DIR_ . 'outofstockordering/src/korel/Helpers/OosoHelper.php';
        if (Configuration::isCatalogMode() && !defined('_PS_ADMIN_DIR_')) {
            return false;
        }
        foreach ($this->getProducts() as $product) {
            if (
                !$this->allow_seperated_package &&
                !$product['allow_oosp'] &&
                StockAvailable::dependsOnStock($product['id_product']) &&
                $product['advanced_stock_management'] &&
                (bool)Context::getContext()->customer->isLogged() &&
                ($delivery = $this->getDeliveryOption()) &&
                !empty($delivery)
            ) {
                $product['stock_quantity'] = StockManager::getStockByCarrier(
                    (int)$product['id_product'],
                    (int)$product['id_product_attribute'],
                    $delivery
                );
            }
            if (
                !$product['active'] ||
                !$product['available_for_order'] ||
                (!$product['allow_oosp'] && $product['stock_quantity'] < $product['cart_quantity'])
            ) {
                $id_product_attribute = $product['id_product_attribute'];
                $id_product = $product['id_product'];
                if ($id_product_attribute !== null && $id_product_attribute !== "0") {
                    $expectedStock = \korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStockPerCombination($id_product_attribute);
                } else {
                    $expectedStock = \korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStock($id_product);
                }
                if( ($product['stock_quantity'] + $expectedStock) < $product['cart_quantity'] )
                    return $returnProductOnFailure ? $product : false;
            }
            if (!$product['allow_oosp']) {
                $productQuantity = Product::getQuantity(
                    $product['id_product'],
                    $product['id_product_attribute'],
                    null,
                    $this,
                    $product['id_customization']
                );
                if ($productQuantity < 0) {
                    return $returnProductOnFailure ? $product : false;
                }
            }
        }
        return true;
    }
}