<?php
/**
 * Korel
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact levikore93@gmail.com
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */

class StockAvailable extends StockAvailableCore
{
    public static function getQuantityAvailableByProduct($id_product = null, $id_product_attribute = null, $id_shop = null)
    {
        require_once _PS_MODULE_DIR_ . 'outofstockordering/src/korel/Helpers/OosoHelper.php';
        if ($id_product_attribute === null) {
            $id_product_attribute = 0;
        }
        $query = new DbQuery();
        $query->select('SUM(quantity)');
        $query->from('stock_available');
        if ($id_product !== null) {
            $query->where('id_product = ' . (int)$id_product);
        }
        $query->where('id_product_attribute = ' . (int)$id_product_attribute);
        $query = StockAvailable::addSqlShopRestriction($query, $id_shop);
        $result = (int)Db::getInstance()->getValue($query);

        if ($id_product_attribute != 0) {
            $expectedStock = \korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStockPerCombination($id_product_attribute);
        } else {
            $expectedStock = \korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStock($id_product);
        }
        if(defined('_PS_ADMIN_DIR_')){
            return $result;
        }
        return $result + $expectedStock;
    }
}