<?php
/**
 * Korel 2020
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact levikore93@gmail.com
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */

namespace Korel\Helpers;

use PrestaShop\PrestaShop\Adapter\Configuration;
use PrestaShop\PrestaShop\Adapter\Entity\Db;
use PrestaShop\PrestaShop\Adapter\Entity\DbQuery;
use Product;

/**
 * Class OosoHelper
 */
class OosoHelper
{
    /**
     * Return the orderable quantity above the default stock for products with combination.
     * If the product cannot be ordered from supplier stock, then return false
     *
     * @param int $id_product_attribute Product combination ID
     *
     * @return bool|int
     */
    public static function getOrderableQuantityAboveDefaultStockPerCombination($id_product_attribute)
    {
        if ($id_product_attribute === null) {
            $id_product_attribute = 0;
        }
        if ($id_product_attribute && !is_null($id_product_attribute)) {
            $sql = new DbQuery();
            $sql->select('quantity');
            $sql->from('outofstockordering', 'ooso');
            $sql->where('ooso.id_product_attribute = ' . (int)$id_product_attribute . '');
            $result = Db::getInstance()->getValue($sql);
            $quantity = 0;
            if (isset($result) && !is_null($result)) {
                $quantity = $result;
            }
            return (int)$quantity;
        }

        return false;
    }

    /**
     * Return the orderable quantity above the default stock for products without combination.
     * If the product cannot be ordered from supplier stock, then return false
     *
     * @param int $id_product Product ID
     *
     * @return bool|int
     */
    public static function getOrderableQuantityAboveDefaultStock($id_product)
    {
        if ($id_product && !is_null($id_product)) {
            $result = Db::getInstance()->getValue('SELECT SUM(`quantity`) FROM `' . _DB_PREFIX_ . 'outofstockordering` WHERE `id_product` = ' . (int)$id_product);
            return (int)$result;
        }

        return false;
    }

    /**
     * Return the stock quantity from default prestashop stock_available table
     *
     * @param $id_product
     * @param null $id_product_attribute
     * @return false|string|null
     */
    public static function oosoGetProductStock($id_product, $id_product_attribute = null)
    {
        $sql = new DbQuery();
        $sql->select('quantity');
        $sql->from('stock_available', 'sa');
        $sql->where('sa.id_product =' . (int)$id_product . ' AND sa.id_product_attribute = ' . (int)$id_product_attribute);
        $result = Db::getInstance()->getValue($sql);

        return $result;
    }

    /**
     * Update the quantity in table outofstockordering
     *
     * @param $id_product
     * @param $id_product_attribute
     * @param $quantity
     * @return bool
     */
    public static function updateOosoStock($id_product, $id_product_attribute, $quantity)
    {
        if (is_null($id_product_attribute)) {
            $id_product_attribute = 0;
        }
        $sql = "UPDATE " . _DB_PREFIX_ . "outofstockordering SET `quantity` = ( `quantity` + " . (int)$quantity . ') WHERE `id_product` = ' . $id_product . ' AND `id_product_attribute` = ' . $id_product_attribute . ' ';
        return Db::getInstance()->execute($sql);
    }

    public static function updateOosoStockFromBackoffice($id_product, $id_product_attribute, $quantity)
    {
        if (is_null($id_product_attribute)) {
            $id_product_attribute = 0;
        }
        $sql = "UPDATE " . _DB_PREFIX_ . "outofstockordering SET `quantity` =  " . (int)$quantity . ' WHERE `id_product` = ' . $id_product . ' AND `id_product_attribute` = ' . $id_product_attribute . ' ';
        return Db::getInstance()->execute($sql);
    }

    /**
     * Set the stock value to 0 in stock_available table
     *
     * @param $id_product
     * @param $id_product_attribute
     * @return bool
     */
    public static function setStockToDefault($id_product, $id_product_attribute = null)
    {
        if (is_null($id_product_attribute)) {
            $id_product_attribute = 0;
        }
        $sql = "UPDATE " . _DB_PREFIX_ . 'stock_available SET `quantity` = 0 WHERE `id_product`=' . $id_product . ' AND `id_product_attribute` = ' . $id_product_attribute . ' ';
        return Db::getInstance()->execute($sql);
    }

    /**
     * Return true if the product exist in the outofstockordering table
     * Return false if the product does't exist in the outofstockordering table
     *
     * @param $id_product
     * @param null $id_product_attribute
     * @return bool
     */
    public function checkIfExist($id_product, $id_product_attribute = null)
    {
        if ($id_product_attribute == null) {
            $id_product_attribute = 0;
        }
        $sql = 'SELECT 1 FROM ' . _DB_PREFIX_ . 'outofstockordering WHERE `id_product`=' . $id_product . ' AND `id_product_attribute` = ' . $id_product_attribute . ' ';
        $result = Db::getInstance()->getValue($sql);
        if ($result) {
            return true;
        }
        return false;
    }

    /**
     * Create a record in the outofstockordering table, based on the $id_product, $id_product_attribute and $quantity parameters
     *
     * @param $id_product
     * @param null $id_product_attribute
     * @return bool
     */
    public function createRecord($id_product, $id_product_attribute = null, $quantity = null)
    {
        if ($id_product_attribute == null) {
            $id_product_attribute = 0;
        }
        if ($quantity == null) {
            $quantity = 0;
        }
        $sql = 'INSERT INTO ' . _DB_PREFIX_ . 'outofstockordering (`id_product`, `id_product_attribute`, `quantity`, `date_add` ) VALUES (' . $id_product . ', ' . $id_product_attribute . ', ' . $quantity . ' , ' . date("Y-m-d") . ') ';
        return Db::getInstance()->execute($sql);
    }

    /**
     * Return the attribute name based on the $id_product, $id_product_attribute and $id_lang
     *
     * @param $id_product_attribute
     * @param $id_product
     * @return string
     */
    public function getAttributeName($id_product, $id_product_attribute, $id_lang)
    {
        $output = '';
        $product = new Product($id_product);
        $attibute_values = $product->getAttributeCombinationsById($id_product_attribute, $id_lang, true);

        foreach ($attibute_values as $attibute) {
            $output .= $attibute['group_name'] . ' - ' . $attibute['attribute_name'] . ' , ';
        }

        return $output;
    }

    /**
     * Update the quantity in outofstockordering table under the import process
     *
     * @param null $importParameters
     * @return bool
     */
    public static function updateOosoStockFromImport($importParameters = null)
    {
        $reference = null;
        if (isset($importParameters['Reference'])) {
            $reference = $importParameters['Reference'];
        } elseif (isset($importParameters['squ'])) {
            $reference = $importParameters['squ'];
        }
        $quantity = null;
        $columnName = '';
        if (!is_null(Configuration::get('OUTOFSTOCKORDERING_CUSTOM_QUANTITY_COLUMN'))) {
            $columnName = Configuration::get('OUTOFSTOCKORDERING_CUSTOM_QUANTITY_COLUMN');
        } else {
            $columnName = 'Custom Quantity';
        }
//        if(isset($importParameters['quantity2'])){
//            $quantity = $importParameters['quantity2'];
//        }
        if (isset($importParameters[$columnName])) {
            $quantity = $importParameters[$columnName];
        }
        $product = null;
        $id_product = null;
        if (!is_null($reference)) {
            $id_product = OosoHelper::getProductByReference($reference);
            if (OosoHelper::checkIfExist($id_product)) {
                OosoHelper::updateOosoStockFromBackoffice($id_product, null, $quantity);
            } else {
                OosoHelper::createRecord($id_product, null, $quantity);
            }

            /*Category modification*/
            $product = new Product($id_product);
            if (isset($importParameters['status_status'])) {
                if ($importParameters['status_status'] = '2') {
                    $product->deleteCategory(11);
                } elseif ($importParameters['status_status'] = '0') {
                    $product->addToCategories(11);
                }
            }
            return true;
        }

        return false;

    }

    /**
     * Return the product_id by product reference
     *
     * @param $reference
     * @return bool|false|string|null
     */
    public function getProductByReference($reference)
    {
        if (empty($reference))
            return false;

        $sql = 'SELECT id_product FROM ' . _DB_PREFIX_ . 'product WHERE reference = "' . $reference . '" ';
        $tab = Db::getInstance()->getValue($sql);
        return $tab;
    }
}