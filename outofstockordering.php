<?php
/**
 * Korel 2020
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact levikore93@gmail.com
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */

if (!defined('_PS_VERSION_')) {
    exit;
}

class Outofstockordering extends Module
{
    public $tabs = array(
        array(
            'name' => 'Ooso Config',
            'class_name' => 'AdminOosoConfig',
            'visible' => true,
            'parent_class_name' => 'AdminParentModulesSf',
        ),
    );

    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'outofstockordering';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'KoreLewi';
        $this->need_instance = 0;

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Enable ordering out of stock products, with quantity LIMIT');
        $this->description = $this->l(' Enable ordering out of stock products, with quantity LIMIT');

        $this->confirmUninstall = $this->l('Do you really want to unistall this awesome module?');

        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        require_once $this->getLocalPath() . 'vendor/autoload.php';
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('OUTOFSTOCKORDERING_LIVE_MODE', false);

        include(dirname(__FILE__) . '/sql/install.php');

        return parent::install() &&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayOrderConfirmation') &&
            $this->registerHook('displayAdminProductsExtra') &&
            $this->registerHook('displayProductAdditionalInfo');
    }

    public function uninstall()
    {
        Configuration::deleteByName('OUTOFSTOCKORDERING_LIVE_MODE');

        include(dirname(__FILE__) . '/sql/uninstall.php');

        return parent::uninstall();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitOutofstockorderingModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path . 'views/templates/admin/configure.tpl');

        return $this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitOutofstockorderingModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            . '&configure=' . $this->name . '&tab_module=' . $this->tab . '&module_name=' . $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        return array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs',
                ),
                'input' => array(
//                    array(
//                        'type' => 'switch',
//                        'label' => $this->l('Live mode'),
//                        'name' => 'OUTOFSTOCKORDERING_LIVE_MODE',
//                        'is_bool' => true,
//                        'desc' => $this->l('Use this module in live mode'),
//                        'values' => array(
//                            array(
//                                'id' => 'active_on',
//                                'value' => true,
//                                'label' => $this->l('Enabled')
//                            ),
//                            array(
//                                'id' => 'active_off',
//                                'value' => false,
//                                'label' => $this->l('Disabled')
//                            )
//                        ),
//                    ),
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-beaker"></i>',
                        'desc' => $this->l('Name of the custom quantity column.'),
                        'name' => 'OUTOFSTOCKORDERING_CUSTOM_QUANTITY_COLUMN',
                        'label' => $this->l('Custom quantity field name'),
                    ),
                ),
                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'OUTOFSTOCKORDERING_CUSTOM_QUANTITY_COLUMN' => Configuration::get('OUTOFSTOCKORDERING_CUSTOM_QUANTITY_COLUMN', false),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be loaded in the BO.
     */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path . 'views/js/back.js');
            $this->context->controller->addCSS($this->_path . 'views/css/back.css');
        }

        if ('AdminProducts' == $this->context->controller->controller_name) {
            Media::addJsDef(
                array(
//                    'ajaxUrl' => $this->context->link->getAdminLink('AdminWebpgeneratorRegenerate'),
                    'ajaxUrl' => _MODULE_DIR_ . 'outofstockordering/ajax.php',
                )
            );

            $this->context->controller->addJquery();
            $this->context->controller->addJS($this->_path . 'views/js/jquery.min.js');
            $this->context->controller->addCSS($this->_path . 'views/css/toastr.css');
            $this->context->controller->addJS($this->_path . 'views/js/ooso.js');
            $this->context->controller->addJS($this->_path . 'views/js/toastr.min.js');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {
        $this->context->controller->addJS($this->_path . '/views/js/front.js');
        $this->context->controller->addCSS($this->_path . '/views/css/front.css');
    }

    /**
     * @param $parameters
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     */
    public function hookDisplayOrderConfirmation($parameters)
    {
        $order = $parameters['order'];
        $products = $order->getProducts();
        foreach ($products as $product) {
            $id_product = $product['product_id'];
            if (isset($product['product_attribute_id'])) {
                $id_product_attribute = $product['product_attribute_id'];
            } else {
                $id_product_attribute = 0;
            }
            $current_stock = \Korel\Helpers\OosoHelper::oosoGetProductStock($id_product, $id_product_attribute);
            if ($current_stock < 0) {
                if (\Korel\Helpers\OosoHelper::updateOosoStock($id_product, $id_product_attribute, $current_stock)) {
                    \Korel\Helpers\OosoHelper::setStockToDefault($id_product, $id_product_attribute);
                }
            }
        }
    }

    /**
     * @param $params
     */
    public function hookDisplayAdminProductsExtra($parameters)
    {
        $id_product = $parameters['id_product'];
        $id_lang = $this->context->language->id;
        $product = new Product($id_product);
        $hasCombination = $product->hasCombinations();
        $quantity = array();
        if ($hasCombination) {
            $attributeSql = 'SELECT DISTINCT `id_product_attribute` FROM ' . _DB_PREFIX_ . 'product_attribute WHERE `id_product` =' . $id_product . '  ';
            $attributeIds = Db::getInstance()->executeS($attributeSql);
            foreach ($attributeIds as $attributeId) {
                $id_product_attribute = $attributeId['id_product_attribute'];
                $attribute_name = \Korel\Helpers\OosoHelper::getAttributeName($id_product, $id_product_attribute, $id_lang);
                if (\Korel\Helpers\OosoHelper::checkIfExist($id_product, $id_product_attribute)) {
                    $helper['id_product'] = $id_product;
                    $helper['id_product_attribute'] = $id_product_attribute;
                    $helper['quantity'] = \Korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStockPerCombination($id_product, $id_product_attribute);
                    $helper['name'] = $attribute_name;
                    $quantity[] = $helper;
                } else {
                    try {
                        \Korel\Helpers\OosoHelper::createRecord($id_product, $id_product_attribute);
                        $helper['id_product'] = $id_product;
                        $helper['id_product_attribute'] = $id_product_attribute;
                        $helper['quantity'] = \Korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStockPerCombination($id_product, $id_product_attribute);
                        $helper['name'] = $attribute_name;
                        $quantity[] = $helper;
                    } catch (Exception $e) {
                        echo 'Caught exception: ', $e->getMessage(), "\n";
                    }
                }
            }
        } else {
            if (\Korel\Helpers\OosoHelper::checkIfExist($id_product)) {
                $helper['id_product'] = $id_product;
                $helper['quantity'] = \Korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStock($id_product);
                $helper['name'] = '';
                $quantity[] = $helper;
            } else {
                try {
                    \Korel\Helpers\OosoHelper::createRecord($id_product);
                    $helper['id_product'] = $id_product;
                    $helper['quantity'] = 0;
                    $helper['name'] = '';
                    $quantity[] = $helper;
                } catch (Exception $e) {
                    echo 'Caught exception: ', $e->getMessage(), "\n";
                }

            }
        }

        $this->smarty->assign(array(
            'quantity' => $quantity,
            'hasCombination' => $hasCombination
        ));

        return $this->display(__FILE__, '/views/templates/admin/ooso_product_extra.tpl');
    }

    public function hookDisplayProductAdditionalInfo($params)
    {
        $id_product = 0;
        if (isset($params['product'])) {
            $id_product = $params['product']->id_product;
            $quantity = \Korel\Helpers\OosoHelper::getOrderableQuantityAboveDefaultStock($id_product);

            $this->smarty->assign(array(
                'quantity' => $quantity,
            ));

            return $this->display(__FILE__, '/views/templates/hook/ooso_product_additional.tpl');
        }

    }
}
