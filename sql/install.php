<?php
/**
 * Korel 2020
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 * If you need help please contact levikore93@gmail.com
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */
$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `' . _DB_PREFIX_ . 'outofstockordering` (
    `id_outofstockordering` int(11) NOT NULL AUTO_INCREMENT,
    `id_product` int(10) NOT NULL,
    `id_product_attribute` int(10),
    `quantity` int(10),
    `date_add` DATETIME NOT NULL,
    PRIMARY KEY  (`id_outofstockordering`)
) ENGINE=' . _MYSQL_ENGINE_ . ' DEFAULT CHARSET=utf8;';

foreach ($sql as $query) {
    if (Db::getInstance()->execute($query) == false) {
        return false;
    }
}
