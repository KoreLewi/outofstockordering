<?php
/**
 * Korel
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 *
 * @author    Korel <levikore93@gmail.com>
 * @copyright Korel
 * @license   commercial
 */

require_once(dirname(__FILE__) . '/../../config/config.inc.php');
require_once _PS_MODULE_DIR_ . 'outofstockordering/src/korel/Helpers/OosoHelper.php';

$id_product = $_POST['id_product'];
$quantity = $_POST['quantity'];
try {
    if(!is_null($id_product)) {
        if(\Korel\Helpers\OosoHelper::updateOosoStockFromBackoffice($id_product, null, $quantity)){
            echo true;
        }
    }
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}
