/**
 * Korel
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Commercial License
 * you can't distribute, modify or sell this code
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file
 *
 * @author    Korel
 * @license   commercial
 */
function saveOosoProduct(idProduct, quantity) {
    var postData = {
        "id_product" : idProduct,
        "quantity" : quantity
    }
    $.ajax({
        type: "POST",
        url: ajaxUrl,
        data: postData ,
        success: function (response) {
            if (response) {
                toastr.success('Done');
            } else {
                toastr.error('Error. Try Again');
            }
        },
        error: function (error) {
            // toastr.error('Product' + ' image regeneration failed');
        }
    });
}