{if $hasCombination}
    <div class="panel">
        <div class="panel-body">
            <div class="btn-group" role="group">
                <table class="table">
                    <thead class="thead-default" id="combinations_thead">
                    <tr>
                        <th></th>
                        <th>Combinations</th>
                        <th>Quantity</th>
                    </tr>
                    </thead>
                    <tbody class="js-combinations-list panel-group accordion">
                    {foreach from=$quantity item=combination}
                        <tr>
                            <td width="1%"
                            </td>
                            <td> {$combination['name']}</td>
                            <td>
                                <input type="number"
                                       id="{$combination['id_product']}_{$combination['id_product_attribute']}" name=""
                                       required="required"
                                       class="form-control" value="{$combination['quantity']}">
                            </td>
                        </tr>
                    {/foreach}
                    </tbody>
                </table>
            </div>
            <button type="post" name="ooso_product_save"><i
                        class="process-icon-loading"></i> {l s='Save'}</button>
        </div>
    </div>
{else}
    <div class="panel">
        <div class="panel-body">
            <div class="btn-group" role="group">
                <input type="number"
                       id="custom_quantity" name="product_{$quantity[0]['id_product']}"
                       required="required"
                       class="form-control" value="{$quantity[0]['quantity']}">
            </div>
            <div class="btn-group pull-right float-right" role="group">
                <a href="#" class="btn btn-primary" onclick="saveOosoProduct( {$quantity[0]['id_product']} , $('#custom_quantity').val() ); return false;">
                    <i class="process-icon-configure" aria-hidden="true"></i>
                    {l s='Save quantity' mod='outofstockordering'}
                </a>
            </div>
        </div>
    </div>
{/if}
